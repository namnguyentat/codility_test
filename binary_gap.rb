def solution n
  n = n.to_s(2)
  return 0 unless n =~ /(0{1,})/
  array = n.split(/(0{1,})/)
  array.delete_at(-1)
  array.map{|x| (x =~ /0{1,}/ ? x.length : 0)}.max
end