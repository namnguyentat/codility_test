def solution a
  stack = []
  matches = {'}' => '{', ']' => '[', ')' => '('}
  a.each_char do |c|
    if c =~ /[{[(]/
      stack.push c
    else
      return 0 if stack.length == 0 || matches[c] != stack.pop
    end
  end

  return 1 if stack.length == 0
  return 0
end


def solution(a, b)
  alive_count = 0
  downstream = []
  downstream_count = 0

  a.each_with_index do |value, index|
    if b[index] == 1
      downstream.push(value)
      downstream_count += 1
    else
      while downstream_count != 0
        if downstream[-1] < value
            downstream_count -= 1
            downstream.pop
        else
          break
        end
      end
      alive_count += 1 if downstream_count == 0
    end
  end
  alive_count += downstream.length

  return alive_count
end

def solution(h)
  stack = []
  block_count = 0    # The number of needing blocks

  for height in h
      while stack.length != 0 and height < stack[-1]
        # If the height of current block is less than
        #    the previous ones, the previous ones have
        #    to end before current point. They have no
        #    chance to exist in the remaining part.
        # So the previous blocks are completely finished.
        stack.pop()
        block_count += 1
      end

      if stack.length == 0 or height > stack[-1]
          # If the height of current block is greater than
          #    the previous one, a new block is needed for
          #    current position.
        stack.push(height)
      end

      # Else (the height of current block is same as that
      #    of previous one), they should be combined to
      #    one block.
  end
  # Some blocks with different heights are still in the stack.
  block_count += stack.length

  return block_count
end

def solution(s)
  matches = {')' => '('}
  stack = []
  s.each_char do |x|
    if x =~ /[\(]/
      stack.push(x)
    else
      return 0 if stack.empty? || matches[x] != stack.pop
    end
  end

  stack.empty? ? 1 : 0
end