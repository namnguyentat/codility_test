def solution a, k
  l = a.length
  b = Array.new(l)
  a.each_with_index do |x, index|
    b[(k+index) % l] = x
  end
  b
end


def solution a
  b = {}

  a.each do |x|
    if b[x]
      b[x] += 1
    else
      b[x] = 1
    end
  end

  b.find{|k, v| v.odd?}.first
end