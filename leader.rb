def solution(A):
    A_len = len(A)
    candidate = -1
    candidate_count = 0
    candidate_index = -1

    # Find out a leader candidate
    for index in xrange(A_len):
        if candidate_count == 0:
            candidate = A[index]
            candidate_index = index
            candidate_count += 1
        else:
            if A[index] == candidate:
                candidate_count += 1
            else:
                candidate_count -= 1

    # Make sure the candidate is the leader
    leader_count = len([number for number in A if number == candidate])
    if leader_count <= A_len//2:
        # The candidate is not the leader
        return 0
    else:
        leader = candidate

    equi_leaders = 0
    leader_count_so_far = 0
    for index in xrange(A_len):
        if A[index] == leader:
            leader_count_so_far += 1
        if leader_count_so_far > (index+1)//2 and
           leader_count-leader_count_so_far > (A_len-index-1)//2:
            # Both the head and tail have leaders of the same value
            # as "leader"
            equi_leaders += 1

    return equi_leaders


def solution(a)
  len = a.length
  candidate = -1
  candidate_pos = -1
  candidate_count = 0
  a.each_with_index do |v, index|
    if candidate_count == 0
      candidate = v
      candidate_pos = index
      candidate_count += 1
    else
      if candidate == v
        candidate_count += 1
      else
        candidate_count -= 1
      end
    end
  end

  leader_count = a.select { |x| x == candidate }.length

  if leader_count <= len/2
    return -1
  else
    a.index{|x| x == candidate}
  end
end