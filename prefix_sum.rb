def solution a
  sum = a.inject(0, :+)
  return 0 if sum == 0
  count = 0
  a.each do |x|
    if x == 1
      sum -= 1
      return count if sum == 0
    else
      count += sum
      return -1 if count > 1_000_000_000
    end
  end
  count
end

def solution a, b, k
  count = b / k - a / k
  count += 1 if a % k == 0
  count
end


def solution(s, p, q)
  m = p.length
  result = []
  m.times do |k|
    x = s[p[k]..q[k]]
    if x.include? "A"
      result << 1
    elsif x.include? "C"
      result << 2
    elsif x.include? "G"
      result << 3
    elsif x.include? "T"
      result << 4
    end
  end
  result
end


def solution(a)
  min_avg_value = (a[0] + a[1]) / 2.0
  min_avg_pos = 0
  (0..a.length-3).each do |index|
    if (a[index] + a[index + 1]) / 2.0 < min_avg_value
      min_avg_value = (a[index] + a[index + 1]) / 2.0
      min_avg_pos = index
    end

    if (a[index] + a[index + 1] + a[index + 2]) / 3.0 < min_avg_value
      min_avg_value = (a[index] + a[index + 1] + a[index + 2]) / 3.0
      min_avg_pos = index
    end
  end

  if (a[-1] + a[-2]) / 2.0 < min_avg_value
    min_avg_pos = a.length - 2
  end

  min_avg_pos
end