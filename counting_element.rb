def solution(x, a)
  result = {}
  (1..x).each {|n| result[n] = n}
  count = 0
  a.each_with_index do |n, index|
    count = count + 1 if result.delete(n)
    return index if count == x
  end
  return -1
end

def solution a
  length = a.length
  return 0 if a.uniq.length < length
  array = (1..length).to_a
  if (a - array).empty?
    1
  else
    0
  end
end

def solution a
  l = a.length
  result  = ((1..l).to_a - a)
  if result.empty?
    l + 1
  else
    result[0]
  end
end

def solution n, a
  max = 0
  result = {}
  (1..n).each do |index|
    result[index] = 0
  end

  a.each do |x|
    if  x < n + 1
      result[x] += 1
    else
      max = result.values.max
      result = {}
      (1..n).each do |index|
        result[index] = max
      end
    end
  end

  result.values
end