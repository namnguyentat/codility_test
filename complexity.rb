def solution x,y,d
  ((y-x)*1.0/d).ceil
end


def solution a
  sum = a.inject(0, :+)
  length = a.length
  sub = 0
  result = []

  (1..length-1).each do |l|
    sub += a[l-1]
    result << (sum - 2*sub).abs
  end

  result.min
end


def solution a
  ((1..a.length+1).to_a - a)[0]
end