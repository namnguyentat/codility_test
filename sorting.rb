def solution(a)
  a.sort!
  (0..a.length-3).each do |index|
    return 1 if a[index] + a[index + 1] > a[index + 2]
  end
  return 0
end

def solution(a)
  a.uniq.count
end

def solution(a)
  a.sort!
  max_pos_value = a[-3] * a[-2] * a[-1]
  max_nag_value = a[0] * a[1] * a[-1]

  max_pos_value > max_nag_value ? max_pos_value :  max_nag_value
end

def solution(a)
  count = 0
  l = a.length
  for i in (0..l-2)
    for j in (i+1..l-1)
      count += 1 if i + a[i] >= j - a[j]
      return -1 if count > 10_000_000
    end
  end
  count
end